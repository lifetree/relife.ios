//
//  ViewController.swift
//  relife
//
//  Created by i.burlak on 09.05.16.
//  Copyright © 2016 i.burlak. All rights reserved.
//

import UIKit
import Material
import Alamofire

class AuthController: UIViewController {
    @IBOutlet var emailField: TextField!
    @IBOutlet var pwdField: TextField!
    @IBOutlet var toggleBtn: UIButton!
    @IBOutlet var sumbitBtn: MaterialButton!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var authDialog: UIView!
   
    var activeDialog: UIView!
    var session: SessionService = SessionService()
    
    override func viewDidLoad() {
        activity.hidden = true;
        pwdField.secureTextEntry = true;
        super.viewDidLoad();
        
        if session.isSaved() {
            
            session.check({ status in
                
                if status == true {
                    self.goToTasks()
                } else {
                    self.showDialog(self.authDialog);
                }
                
            })
            
        } else {
            showDialog(self.authDialog);
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }

    func showDialog(dialog: UIView!) {
        self.view.addSubview(dialog)
        
        dialog.centerXAnchor.constraintEqualToAnchor(view.centerXAnchor).active = true
        let lConstraint = dialog.bottomAnchor.constraintEqualToAnchor(view.topAnchor, constant: 352)
        
        NSLayoutConstraint.activateConstraints([lConstraint])
        view.layoutIfNeeded()
        
        dialog.alpha = 0
        UIView.animateWithDuration(0.5) {
            dialog.alpha = 0.8
        }
    
    }

    func goToTasks() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let tasksView = storyBoard.instantiateViewControllerWithIdentifier("TasksView") as! TasksController
        
        self.presentViewController(tasksView, animated:true, completion:nil)
    }
    
    func disableForm(state: Bool) {
        emailField.enabled = !state;
        pwdField.enabled = !state;
        sumbitBtn.enabled = !state;
        toggleBtn.enabled = !state;
    
        if state {
            activity.hidden = false;
            activity.startAnimating();
        } else {
            activity.hidden = true;
            activity.stopAnimating();
        }
    }
    
    func showErrors(errors: [(String, String)]) {
        var field: TextField? = nil;
        
        for err in errors {
            
            if err.0 == "pwd" {
                field = pwdField;
            }
            
            if err.0 == "email" {
                field = emailField;
            }
            
            field?.detail = err.1
        }
        
    }
    
    @IBAction func onEmailChange(sender: TextField) {
        sender.detail = "";
    }
    
    @IBAction func onPwdChange(sender: TextField) {
        sender.detail = "";
    }
    
    @IBAction func onSubmit(sender: MaterialButton) {
        var errors: [(String, String)] = [];
  
        if emailField.text == "" {
            errors.append(("email", "Введите логин пользователя"));
        }
        
        if pwdField.text == "" {
            errors.append(("pwd", "Введите пароль пользователя"));
        }
        
        if errors.count > 0 {
            showErrors(errors);
            return
        }
        
        disableForm(true);
        
        session.login(emailField.text!, pwd: pwdField.text!, onCompleted: { errors in
            self.disableForm(false);
            
            if errors.count > 0 {
                self.showErrors(errors);
                return;
            }
            
            self.goToTasks()
        });
        
    }

}

