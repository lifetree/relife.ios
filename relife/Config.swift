//
//  Config.swift
//  relife
//
//  Created by i.burlak on 09.05.16.
//  Copyright © 2016 i.burlak. All rights reserved.
//

import Foundation

struct Config {
    var baseUrl = "https://relifeapi-imagemap.rhcloud.com/api/";
}