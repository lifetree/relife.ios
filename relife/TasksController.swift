//
//  TasksController.swift
//  relife
//
//  Created by i.burlak on 10.05.16.
//  Copyright © 2016 i.burlak. All rights reserved.
//

import UIKit
import Material
import Alamofire

class TasksController: UIViewController {
    var tasks = TasksService()
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.renderToolbar()
        self.renderTasks();
        
        
        tasks.refresh { (errors) in
            
            if (errors.count > 0) {
                return print(errors);
            }
            
            self.renderTasks();
        }
        
        
    }
    
    func renderToolbar () {
        // Title label.
        let titleLabel: UILabel = UILabel()
        titleLabel.text = "Material"
        titleLabel.textAlignment = .Left
        titleLabel.textColor = MaterialColor.white
        
        // Detail label.
        let detailLabel: UILabel = UILabel()
        detailLabel.text = "Build Beautiful Software"
        detailLabel.textAlignment = .Left
        detailLabel.textColor = MaterialColor.white
        
        var image: UIImage? = MaterialIcon.menu
        
        // Menu button.
        let menuButton: FlatButton = FlatButton()
        menuButton.pulseColor = MaterialColor.white
        menuButton.tintColor = MaterialColor.white
        menuButton.setImage(image, forState: .Normal)
        menuButton.setImage(image, forState: .Highlighted)
        
        // Switch control.
        let switchControl: MaterialSwitch = MaterialSwitch(state: .Off, style: .LightContent, size: .Small)
        
        // Search button.
        image = MaterialIcon.search
        let searchButton: FlatButton = FlatButton()
        searchButton.pulseColor = MaterialColor.white
        searchButton.tintColor = MaterialColor.white
        searchButton.setImage(image, forState: .Normal)
        searchButton.setImage(image, forState: .Highlighted)
        
        let toolbar: Toolbar = Toolbar()
        toolbar.statusBarStyle = .LightContent  
        toolbar.backgroundColor = MaterialColor.blue.base
        toolbar.titleLabel = titleLabel
        toolbar.detailLabel = detailLabel
        toolbar.leftControls = [menuButton]
        toolbar.rightControls = [switchControl, searchButton]
        
        view.addSubview(toolbar)
    }
    
    func renderTasks() {
        let imageCardView: ImageCardView = ImageCardView()
        
        // Image.
        let size: CGSize = CGSizeMake(UIScreen.mainScreen().bounds.width - CGFloat(40), 150)
        imageCardView.image = UIImage.imageWithColor(MaterialColor.cyan.darken1, size: size)
        
        // Title label.
        let titleLabel: UILabel = UILabel()
        titleLabel.text = "Welcome Back!"
        titleLabel.textColor = MaterialColor.white
        titleLabel.font = RobotoFont.mediumWithSize(24)
        imageCardView.titleLabel = titleLabel
        imageCardView.titleLabelInset.top = 100
        
        // Detail label.
        let detailLabel: UILabel = UILabel()
        detailLabel.text = "It’s been a while, have you read any new books lately?"
        detailLabel.numberOfLines = 0
        imageCardView.detailView = detailLabel
        
        // Yes button.
        let btn1: FlatButton = FlatButton()
        btn1.pulseColor = MaterialColor.cyan.lighten1
        btn1.setTitle("YES", forState: .Normal)
        btn1.setTitleColor(MaterialColor.cyan.darken1, forState: .Normal)
        
        // No button.
        let btn2: FlatButton = FlatButton()
        btn2.pulseColor = MaterialColor.cyan.lighten1
        btn2.setTitle("NO", forState: .Normal)
        btn2.setTitleColor(MaterialColor.cyan.darken1, forState: .Normal)
        
        // Add buttons to left side.
        imageCardView.leftButtons = [btn1, btn2]
        
        // To support orientation changes, use MaterialLayout.
        view.addSubview(imageCardView)
        
        print(123);
        imageCardView.translatesAutoresizingMaskIntoConstraints = false
        MaterialLayout.alignFromTop(view, child: imageCardView, top: 100)
        MaterialLayout.alignToParentHorizontally(view, child: imageCardView, left: 20, right: 20)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }


}
