//
//  Tasks.swift
//  relife
//
//  Created by i.burlak on 10.05.16.
//  Copyright © 2016 i.burlak. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

class Action: Object {
    dynamic var start  = 0
    dynamic var completed = false
    dynamic var failed = false
}

class Task: Object {
    dynamic var id  = ""
    dynamic var ownerId = ""
    dynamic var name = ""
    dynamic var milestoneId = ""
    dynamic var recurrent = false
    dynamic var penalty = false
    
    let actions = List<Action>()
}

class TasksService {
    var list: Results<Task>
    var session: SessionService = SessionService()
    let config = Config()
    
    init() {
        let realm = try! Realm()
        list = realm.objects(Task).filter("ownerId = '\(session.getUser())'")
    }
    
    func refresh(onCompleted: (errors: [(String, String)]) -> Void) {
        let headers = ["Authorization": "Token token=\"\(session.getToken())\""]
        
        Alamofire.request(.GET, config.baseUrl + "users/" + session.getUser() + "/tasks", headers: headers).responseJSON { response in
            switch response.result {
            case .Success(let data):
                let status = response.response?.statusCode;
                
                if status == 401 || status == 403 {
                    return onCompleted(errors: [("auth", "Потеря авторизации")])
                }
                
            
                if response.response?.statusCode != 200 {
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    return onCompleted(errors: [("system", "invalid data")])
                }
                
                print(data);
                
            case .Failure(let error):
                print(error)
                return onCompleted(errors: [("system", "invalid data")])
            }
        };
        
    }
    
}