//
//  Session.swift
//  relife
//
//  Created by i.burlak on 09.05.16.
//  Copyright © 2016 i.burlak. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

class Session: Object {
    dynamic var userid  = ""
    dynamic var token = ""
}

class SessionService {
    var config: Config = Config();
    let data = NSUserDefaults.standardUserDefaults()
    var session = Session()
    
    init() {
        let realm = try! Realm()
        let data = realm.objects(Session)
        
        if data.count > 0 {
            session = data[0] as Session
        } else {
            
            try! realm.write() {
                realm.add(session);
            }
            
        }
    
    }
    
    func getUser() -> String {
        return session.userid;
    }
    
    func getToken() -> String {
        return session.token;
    }
    
    func isSaved () -> Bool {
        
        if session.token != "" {
            return true
        } else {
            return false
        }
        
    }
    
    func login (email: String, pwd: String, onCompleted: (errors: [(String, String)]) -> Void) {
        let credentialData = "\(email):\(pwd)".dataUsingEncoding(NSUTF8StringEncoding)!
        let base64Credentials = credentialData.base64EncodedStringWithOptions([])
        let headers = ["Authorization": "Basic \(base64Credentials)"]

        Alamofire.request(.GET, config.baseUrl + "session", headers: headers).responseJSON { response in
            switch response.result {
            case .Success(let data):
                if response.response?.statusCode != 200 {
                    return onCompleted(errors: [("email", "Вы ввели не существующий email")])
                }
                
                let json = JSON(data)
                let realm = try! Realm()
                
                try! realm.write() {
                    self.session.userid = json["id"].stringValue
                    self.session.token = json["token"].stringValue
                }

            case .Failure(let error):
                print (error);
                return onCompleted(errors: [("system", "Ошибка сервера")])
            }
            
            onCompleted(errors: []);
        };
        
    }
    
    
    func check(onCompleted: (status: Bool) -> Void) {
        let headers = ["Authorization": "Token token=\"\(session.token)\""]
        
        Alamofire.request(.GET, config.baseUrl + "session", headers: headers).responseJSON { response in
            switch response.result {
            case .Success(_):
                
                if response.response?.statusCode != 200 {
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    return onCompleted(status:  false)
                } else {
                    return onCompleted(status:  true)
                }
            case .Failure(let error):
                print(error)
                return onCompleted(status:  false)
            }
        };

    }
    
}